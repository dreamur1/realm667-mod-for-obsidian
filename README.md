# Realm667 Plugin - Obsidian Version

This repo is a fork of the original mod by `mridlen`. It merely contains the updates to the module files I needed in order for Obsidian to load them. Note! You will still need to download the `pk3` files from the [original repo](https://github.com/mridlen/Oblige_Realm667_Mod/tree/master), they will not be hosted here at this time

## How to use?

Add these two `lua` files to your `modules` folder within your obsidian installation directory and then make the desired changes within the UI.

## Credits / Dependencies

- [Oblige_Realm667_Mod](https://github.com/mridlen/Oblige_Realm667_Mod/tree/master) originally made by mridlen to work on Oblige
- [Obsidian Level Generator](https://obsidian-level-maker.github.io/index.html) v21+